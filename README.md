# Bachelor's Thesis for UA 033 661 Astronomy at the University of Vienna
## Python bindings for the UVIE FlightOS process chain

[![CI_LaTeX_Compile](https://gitlab.com/yumasheta/thesis_bachelor_astro/badges/master/pipeline.svg)](https://gitlab.com/yumasheta/thesis_bachelor_astro/pipelines)

## PDF Download Link:

- [Link](https://gitlab.com/yumasheta/thesis_bachelor_astro/-/jobs/artifacts/master/raw/bac_thesis.pdf?job=compile_LaTeX)







