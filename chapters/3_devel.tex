% !TeX root = ../bac_thesis.tex

% Developing pyprocchain
\chapter[%
  Developing \protect\pyprocchainHeader{}
 ]{Developing \lstinline[style=myPython,basicstyle=\ttfamily\Huge]!pyprocchain!}%
\label{ch:py-bindings}

A very useful feature of UVIE FlightOS is the possibility to compile and run the software on a standard PC, ARM based system and of course \acrshortpl{DPU} with Leon3 SPARC architecture.
Especially when validating the \acrshort{IFSW} against a test suite, this allows for the flexibility to simulate the previously discussed hardware components.
The need for accurate simulators results from the limited availability of the final hardware.
\Textcite{smile-iasw} discuss the latest additions to the testing setup that is in use for the development of \acrshort{SMILE}'s flight software.

Another benefit of the broad architecture compatibility is that the development of new software can take place on the PC in a familiar environment.
To separate concerns, it helps to isolate the parts of the software that are being worked on.
In the case of the process chain, that I introduced in \cref{sec:ob-proc-chain}, this means that the developer only works with the processing network itself.
In this state, it is decoupled from the inputs it would receive from the \acrshort{SEM}/\acrshort{FEE} and its output data is handled manually instead of by the \acrshort{OBC}.
The scheduling for the kernels is also controlled by the developer in order to be fully independent of the remaining \acrshort{IASW}.

In the next section, I discuss the necessary code snippets for setting up and executing a processing network in such an isolated environment.
Compared to the full implementation, the ELF binaries of the kernels are reduced to simple functions.
All the details regarding this change are presented by \textcite{leanos-um}.
Notable differences to the ELF binaries are that the kernels themselves have to specify their memory resource requirements as well as their operation ID codes and critical levels as a structure.
When dealing with simple functions these properties are set manually when registering new tracker nodes with the network.

However, it is important to note the aspects that do not change for the decoupled version.
Setting up and adding new processing tasks follows the same scheme.
The code for the actual operation functions also does not change and allows for re-use as part of the ELF binaries.
In the end, the additional code required to replace the removed parts of the \acrshort{IASW} only needs to be provided once.
Afterwards, the development of new operation functions takes place in this constrained environment with strictly separated concerns.

The later sections of this chapter discuss how to improve this development process with the use of the Python bindings provided by \pythoninline{pyprocchain}.
I present the implementation of \pythoninline{pyprocchain} as a Python C extension type and discuss its strengths and issues.
As its main goal \pythoninline{pyprocchain} shall allow to control the process network and add new operation functions from within Python while also being able to fully utilize the existing code written in C.
Still, \pythoninline{pyprocchain} does not remove the need to implement the final version of operation functions as part of the full \acrshort{IASW} in C, like all of the existing source code of UVIE FlightOS.\@


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	section:	The Generic Processing Network
%	label:		sec:generic-proc-net
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The Generic Processing Network}\label{sec:generic-proc-net}

The code snippets presented in this section follow chapter 6.5 of the user manual for UVIE FlightOS \parencite{leanos-um}.
The source code for a fully functional program that demonstrates the use of a generic processing network is included in \cref{app:generic-proc-net}.
In this section, I only discuss the used functions and concepts behind them.
In order to have the most direct control over the processing network, I make use of the C programming language in accordance with the software requirements of the flight software \parencite{leanos-srs}.

The first components to focus on are the operation functions.
Their first parameter is the unique ID code that is associated with the tracker node that the operation function is bound to.
These functions require access to the entity representing the currently processed input task as their second parameter.
In the function body, the input task can be queried for any linked data like the steps lists or the size of the data buffer.
Most importantly, the pointer to the data buffer can be extracted so that these data can be manipulated accordingly.
The return value is used for signals like success and failure that control the next processing steps.
A set of predefined constants with different signal codes is available.
The type definition and signature of the operation functions are listed below.
\medskip%
\begin{C}
	typedef int (*op_func_t)(unsigned long op_code, struct proc_task *);
	int op_function(unsigned long op_code, struct proc_task *t);
\end{C}
\medskip%
After all required operation functions are prepared the processing network can be set up.
The dedicated \cinline{proc_net} structure contains references to all the nodes of the network and is the main access point to control the flow of the processing pipeline.
As part of creating a new \cinline{proc_net}, memory needs to be allocated for the structure itself and its contents.
The special input node is also set up right away because it is standard for all networks.
All of these steps are put together in the function listed below.
\medskip%
\begin{C}
	struct proc_net *pn_create(void);
\end{C}
\medskip%
The empty (except for the input node) network needs to be populated with tracker nodes.
Again, they have their own \cinline{proc_tracker} structure that provides the references to the properties of the node and a list for keeping track of assigned tasks.
Naturally, a reference to the operation function associated with this node is also saved.
A new tracker node can be created with the function listed below that also takes care of memory allocation. The supplied arguments are the operation function pointer, unique ID code and the threshold for the number of tasks above which the node will be prioritized during scheduling cycles.
\medskip%
\begin{C}
	struct proc_tracker *pt_track_create(op_func_t op, unsigned long op_code, size_t n_tasks_crit);
\end{C}
\medskip%
For adding new tracker nodes to the network two cases need to be distinguished.
The output node is special and requires to be added using its own function.
The reference to the output operation function can be passed directly to it and it is not necessary to create a tracker node like before.
There is no critical level and the ID code is fixed internally.
Any other tracker node with a custom operation function needs to be created first and then passed to a different function that adds it to the network.
The functions used to add output and tracker nodes are listed below.
They both require the reference to the process network as their first arguments.
\medskip%
\begin{C}
	int pn_create_output_node(struct proc_net *pn, op_func_t op);
	int pn_add_node(struct proc_net *pn, struct proc_tracker *pt);
\end{C}
\medskip%
At this stage, the network is set up and we can focus on creating input tasks.
Following the already familiar fashion, tasks have their own \cinline{proc_task} structure that provides the references for their linked data.
The function used to create new tasks allocates the necessary memory and populates the structure based on the values of its parameters.
These are the pointer to the data buffer, the size of the data buffer, the number of processing steps and additional metadata.
These values can be modified afterwards.
For example, the data buffer can be prepared and assigned to the task after its creation.
\medskip%
\begin{C}
	struct proc_task *pt_create(void *data, size_t size, size_t steps, unsigned long type, unsigned long seq);
\end{C}
\medskip%
Before we can add the input task to the network, we still need to set its processing steps.
Using the function listed below, the steps are specified via the ID code of the operation to perform.
An additional data pointer for operation specific data is also available.
Multiple steps can be added and will be processed in the order they were added.
It is important that the number of set steps exactly matches the value for \cinline{steps} supplied previously when creating the task.
The return value of the function signals success or failure.
\medskip%
\begin{C}
	int pt_add_step(struct proc_task *t, unsigned long op_code, void *op_info);
\end{C}
\medskip%
The input task can then be added to the network.
The function listed below registers the supplied \cinline{proc_task} structure pointer that represents the fully configured input task with the special input node of the given process network.
\medskip%
\begin{C}
	void pn_input_task(struct proc_net *pn, struct proc_task *t);
\end{C}
\medskip%
The manual management of the process network makes use of the three functions listed below.
All newly added input tasks are collected in the input node.
Calling the first function processes all tasks that are currently assigned to the input node and registers each of them with the first tracker node in their list of steps.
The integer return value signals success or failure.

The second function triggers a scheduling cycle.
If there are no tracker nodes with fill levels above their critical levels, the first, non-empty tracker node is selected.
Otherwise, the first overcritical tracker node is selected.
Then all the tasks that are currently assigned to that node are processed in the order of their assignment.
For each task, the return value of the operation function is evaluated which can be used to intervene in the current cycle.
For example, an operation function can signal to abort the processing of the current tracker node or even to delete the most recent input task.
The \cinline{pn_process_next} function returns the number of tasks the selected node processed which is not necessarily equal to the number of tasks that were assigned to the node at the beginning of the cycle.
Any remaining tasks stay assigned to that node.

The output node can be evaluated with the third function.
At any time during execution, this function can be called and applies the special output operation function to all input tasks that are assigned to the output node at that time.
Again, the number of processed tasks is returned.
\medskip%
\begin{C}
	int pn_process_inputs(struct proc_net *pn);
	int pn_process_next(struct proc_net *pn);
	int pn_process_outputs(struct proc_net *pn);
\end{C}
\medskip%
Any existing processing network can also be destroyed.
However, the corresponding function that is listed below only tears down the network and cleans up all its tracker nodes.
The input tasks themselves are not deleted.
Therefore, it is important to properly clean up the tasks when they are processed at the output node by using the function dedicated to destroying them as part of the special output operation function.
This \cinline{pt_destroy} function does not touch the tasks' data buffers so that they can still be accessed for further use after the task is destroyed.
\vspace{-\baselineskip}
\medskip%
\begin{C}
	void pn_destroy(struct proc_net *pn);
	void pt_destroy(struct proc_task *t);
\end{C}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	section:	Requirements
%	label:		sec:requirements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Requirements}\label{sec:requirements}

Based on the discussion of the implementation in C of the processing network of UVIE FlightOS in the previous chapter (referenced as "back end" from here on) we can now formulate the requirements for the \pythoninline{pyprocchain} Python bindings.
The need for a different interface is brought up because working with the native C implementation is cumbersome and slows down iterative development.
Tools like IPython%
\footnote{\url{https://ipython.org/} (visited on 06/2021)}
or Jupyter%
\footnote{\url{https://jupyter.org/} (visited on 06/2021)\vspace{-2\baselineskip}}
on the other hand, support interactive programming which in theory allows modifying a running processing network with new code.
The Python programming langue in particular is very popular among the Astrophysics community.
With great third party packages like NumPy%
\footnotemark\blfootnote{\hspace{0.46\textwidth}\({}^{\thefootnote}\)\url{https://numpy.org/} (visited on 06/2021)},
% \footnote{\hspace{0.48\textwidth}\url{https://numpy.org/} (visited on 06/2021)},
SciPy%
\footnotemark\blfootnote{\hspace{0.46\textwidth}\({}^{\thefootnote}\)\url{https://www.scipy.org/} (visited on 06/2021)}
% \footnote{\hspace{0.48\textwidth}\url{https://www.scipy.org/} (visited on 06/2021)}
and Astropy%
\footnotemark\blfootnote{\hspace{0.46\textwidth}\({}^{\thefootnote}\)\url{https://www.astropy.org/} (visited on 06/2021)}
% \footnote{\hspace{0.48\textwidth}\url{https://www.astropy.org/} (visited on 06/2021)}
there are a lot of tools available to tackle new problems.
Additionally, the supplemental software for UVIE FlightOS, like the electronic ground support equipment, already makes heavy use of Python as described by \textcite{cheops-um_ifsw}.

The overall goal is to keep as much functionality of the C back end as possible, while still simplifying and optimizing the interface for the specific task of developing new operation functions.
In this context, it shall not matter which language (C or Python) these functions are coded in.
They shall be fully inter-compatible and configurable from the Python interface.
New input tasks shall also be configured from the Python interface.
Their data buffers shall be provided as a Python compatible data type and shall remain accessible from Python at any later time.

Following the modern object-oriented programming paradigm, \pythoninline{pyprocchain} shall provide a class \pythoninline{ProcNet} that represents the processing network.
Methods of this class shall wrap the necessary functionality of the back end discussed in the previous \cref{sec:generic-proc-net}.
Creating a new processing network shall be done by instantiating the \pythoninline{ProcNet} class.
The initial state of the instances shall be configured for convenience, such that in addition to the simple call to \pythoninline{pn_create} the output node is also set up.
The output operation function shall remove assigned tasks from the network, but shall not touch their data buffers.
We refrain from class representations of input tasks and processing trackers because they add additional complexity to the Python bindings.
Instead, the \pythoninline{ProcNet} class shall provide the interface to create and add new input tasks directly.
We allow the details of how exactly processing trackers are handled to be determined by the implementation, knowing that this puts the sophisticated scheduling and load balancing features of the processing network in jeopardy.

To set our expectations accordingly, we summarize what \pythoninline{pyprocchain} shall not be able to do and what therefore requires the use of the original C interface.
The Python bindings are not meant as a replacement for the back end.
We are not trying to reinvent the wheel, but rather to add more buttons to the steering wheel.
What is more, the targeted Leon3 SPARC hardware has very limited support for Python.
In recent efforts, \textcite{micropython-leon,micropython-obcp} managed to use MicroPython%
\footnote{\url{http://micropython.org/} (visited on 06/2021)}
on Leon based platforms, but this implementation of Python is non-standard and comes with many subtleties.

Additionally, we expect to lose some performance.
Partly, this will be the result of just using Python%
\footnote{
	See \url{https://wiki.python.org/moin/PythonSpeed} (visited on 06/2021) for an elaborate discussion of the general performance of Python.
	Because the aforementioned popular third party packages provide tools that are implemented in C they are very performant.
	Of course, we aim to be fully compatible with them.},
but also because we miss out on some of the optimizations of the back end in favor of a simpler interface.
We do not come forward with any further analysis of performance related aspects.
Still, we note that when developing on the PC platform the load generated by the processing network will be very small compared to the capabilities of modern PC hardware.

Finally, \pythoninline{pyprocchain} shall be distributed by the means of a PIPY%
\footnote{\url{https://pypi.org/} (visited on 06/2021)}
package.
This allows to use \pythoninline{pip}%
\footnote{
	\lstinline[style=myPython,basicstyle=\ttfamily\footnotesize]!pip! also allows to install local packages, i.e.\ packages that are not published via PYPI but are saved locally.
	See the documentation at \url{https://pip.pypa.io/en/stable/} (visited on 06/2021).}
for easy configuration of development environments for Python and provides all the necessary tools to specify external dependencies and to build C dependencies like the back end.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	section:	Exploring Implementations using only Python
%	label:		sec:python-only-impl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Exploring Implementations using only Python}\label{sec:python-only-impl}

As a first idea, I discuss the benefits and drawbacks of implementations that only require Python code.
In practice, this translates to Python modules that define the \pythoninline{ProcNet} class and its interface and that take care of wrapping the C functions from the back end.
The obvious advantage is that I can make use of all the language specific features of Python which are part of the reasons I am developing \pythoninline{pyprocchain} in the first place.
However, it is not straightforward how to deal with the complex data structures defined and used by the back end.
There are different approaches to solve this problem that all rely on translating Python and C data types into each other.
Unless this process is intuitive and can successfully be applied to our problem, this extra translation step is not worth it.
Another problem is that the translation requires maintaining two mirrored versions of the data structures, one for each programming language.
This would tightly couple the Python code to the exact implementation of the back end.
In the case of a minor change to the back end, that keeps the interface the same but adjusts the layout of some data structures, it would be necessary to propagate this change manually to the Python code as well.
Clearly, this type of "shotgun surgery" is frowned upon.
It is also important to keep in mind that good support for Python callbacks from the C back end is needed in order to register new operation functions from Python.
In the following, I introduce two popular libraries that provide us with different tools for a Python-only solution.


\subsection*{\lstinline[style=myPython,basicstyle=\ttfamily\large]!ctypes!}

The \pythoninline{ctypes}%
\footnote{\url{https://docs.python.org/3/library/ctypes.html} (visited on 06/2021)}
library is part of the Python standard library and therefore does not add additional dependencies to the project.
The workflow depends on the back end being available as a \acrfull{DLL}.
The \acrshort{DLL} is loaded into Python and is represented as a special class that exposes the library functions as attributes.
It is also possible to set strict signatures for these functions that allow control of the exceptions raised from passing invalid arguments.
If the translation for the data types is configured correctly, any return type of these functions can be managed via Python code.
\pythoninline{ctypes} provides a range of custom types that are used to convert between C and Python native data types.
They can also be combined into structures and unions.
Additionally, custom classes can be configured as C compatible types by specifying a certain attribute that is converted and passed as an argument to the functions loaded from the \acrshort{DLL}.
There is also support for generating pointers to the memory of the custom types in order to pass them to C functions.
Setting up Python callback functions consists of a few steps.
First, a fixed C-like declaration has to be build from \pythoninline{ctypes} types and then the Python function object can be converted to a C function pointer and passed to C functions.
Concerning data types that are provided by NumPy (i.e.\ the \pythoninline{ndarray}), compatibility is ensured by NumPy itself with similar translation mechanisms.
Therefore, the previously mentioned external packages can be used as well.
To sum up, \pythoninline{ctypes} requires a lot of manual "glue code" for converting data to and from the types native to each language.


\subsection*{CFFI - C Foreign Function Interface}

The CFFI%
\footnote{\url{https://readthedocs.org/projects/cffi/} (visited on 06/2021)}
library is an external tool that is available to install via \pythoninline{pip}.
It aims to automate the generation of as much of the required "glue code" as possible.
The idea is to include the declarations of the C functions and structures from the back end in the Python code as simple strings.
These strings are then interpreted by CFFI which creates the necessary type translations automatically.
For most of the fundamental types, this works as intuitively expected and for example C floating-point numbers a mapped to Python's regular float type.
For more complex C structures and pointers, a special \pythoninline{cdata} type exposes the values to Python code.
The major difference to \pythoninline{ctypes} is that CFFI makes use of C syntax instead of providing its own interface.
Concerning the NumPy \pythoninline{ndarray} type, the \pythoninline{cdata} type provides direct access to its data buffer via special methods.
Python callbacks are also supported and require a compatible C declaration in the setup strings.
Custom Python objects can be passed on to the callbacks as well by using a \cinline{void *userdata} parameter and can then be interacted with in the usual way.

Another advantage is the possibility to configure CFFI to use the C source code of the back end directly, instead of relying on precompiled \acrshortpl{DLL}.
The compilation can then be triggered via a Python interface.
As a result, the translation layer becomes available as a C extension%
\footnote{Python C extensions are described in the documentation by \textcite{py3-cext} and are the basis for the implementation of \lstinline[style=myPython,basicstyle=\ttfamily\footnotesize]!pyprocchain! that I present in the next chapter.}
for Python.
This is the most performant option because the "glue code" is only generated once when building the C extension, instead of every time the \acrshort{DLL} is loaded.
This setup process can be easily integrated into a PYPI package, which automatically generates and compiles the necessary C code when being installed.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	section:	Implementation as a Python C Extension
%	label:		sec:c-ext
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Implementation as a Python C Extension Type}\label{sec:c-ext}

% \begin{figure}
% 	\centering
% 	\begin{subfigure}[b]{0.35\textwidth}
% 		\centering
% 		\includegraphics[width=\textwidth,clip,trim={0 2.965in 1.865in 0}]{img_src/pyprocchain_UML_old.pdf}
% 		\caption{Package \pythoninline{pyprocchain}}%
% 		\label{subfig:pack-pyprocchain}
% 	\end{subfigure}
% 	\hfill
% 	\begin{subfigure}[b]{0.61\textwidth}
% 		\centering
% 		\includegraphics[width=\textwidth,clip,trim={0 0 0 1.7in}]{img_src/pyprocchain_UML_old.pdf}
% 		\caption{Class \pythoninline{ProcNet}}%
% 		\label{subfig:class-procnet}
% 	\end{subfigure}
% 	\caption{Diagrams}\label{fig:pyproc-diags}
% \end{figure}

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.48\textwidth}
		\centering
		\def\svgwidth{\textwidth}
		\ttfamily\small
		\input{img_src/pyprocchain_UML.pdf_tex}
		\caption{Package \pythoninline{pyprocchain}}%
		\label{subfig:pack-pyprocchain}
	\end{subfigure}

	\vspace*{2\baselineskip}
	\begin{subfigure}[b]{0.87\textwidth}
		\centering
		\def\svgwidth{\textwidth}
		\ttfamily\small
		\input{img_src/ProcNet_UML.pdf_tex}
		\caption{Class \pythoninline{ProcNet}}%
		\label{subfig:class-procnet}
	\end{subfigure}
	\vspace*{\baselineskip}
	\caption[Summary of \pythoninline{pyprocchain} and \pythoninline{ProcNet} contents.]{%
		Summary of the contents of \pythoninline{pyprocchain} (\subref{subfig:pack-pyprocchain}) and the interface of \pythoninline{ProcNet} (\subref{subfig:class-procnet}).
		The listings make use of Python type hints to describe the members.
		The \pythoninline{pyprocchain} package exposes the \pythoninline{ProcNet} class, the \pythoninline{OPCODES} dictionary with the ID codes of available operation functions from the back end and the \lstinline[style=myPython,basicstyle=\color{emph}\ttfamily\normalsize]!demo! module with example Python code.
		The \pythoninline{ProcNet} class has one attribute \pythoninline{output} that saves a reference to all NumPy arrays of the networks input tasks.
		Instantiation takes no arguments and the steps list of new processing tasks can be specified either by the ID code of an existing operation functions from the back end or by a Python callback function that accepts a single NumPy array as its only argument.
	}\label{fig:pyproc-diags}
\end{figure}

As an alternative to the tools discussed in the previous section, I now present the chosen implementation for \pythoninline{pyprocchain}.
In my efforts, I turn to a solution that is written purely in C.
The most obvious benefit is that I can now interface the back end directly in the way it is supposed to be done.
This prevents the tight coupling of the Python bindings in favor of better maintainable code.
I no longer have to deal with tedious type conversions when calling C functions as all the data structures are native to C.
Still, I can expose a Python interface that is based on the roots of this language.

Python is an interpreted language and of the different implementations for its interpreter, CPython is the standard.
In the chapter \citetitle{py3-cext} of the Python documentation the \textcite{py3-cext} explains how to use CPython to expose a module that is written in C to the interpreter and make use of it from Python.
What is more, it is also possible to fully program a custom C extension type that becomes available as a new type (roughly equivalent to classes) with methods and attributes like any other of the built-in types.
The Python/C \acrfull{API} contains all the available tools to interface Python objects from C as special \cinline{PyObject} structures.
Because there is an equivalent \acrshort{API} call for every interaction with Python objects, working with the Python/C \acrshort{API} is just like working with the objects directly from Python.
Concerning the NumPy \pythoninline{ndarray} type, \textcite{numpy-ref} document the NumPy C-\acrshort{API} which provides an analogous interface to the NumPy package and its types.

The only downside to this approach is Python's reference counting memory management scheme which has to be taken care of manually when using the Python/C \acrshort{API}.
In short, the internal garbage collector of Python cleans up any objects with a reference count of zero, which signifies that there are no variables bound to these objects.
Normally, the interpreter takes care of reference counting, but C extensions and extension types act outside of the interpreter and require manual effort for clean memory management.

The \pythoninline{pyprocchain} extension module exposes our custom \pythoninline{ProcNet} type.
Its underlying \cinline{PyObject} structure reserves one field to the \cinline{proc_net} structure that is returned from the back end when creating a new processing network.
However, this field is hidden for \pythoninline{ProcNet} instances and can only be accessed from the C extension module code.
This allows us to seamlessly manage the complex C structures from the back end as part of Python objects without dealing with type conversions.
Only, there is a minor complication for the processing tasks.
As previously stated, it is required that the data buffers for each processing task shall be provided from Python code.
In order to simplify the process, I restrict compatibility to only the NumPy \pythoninline{ndarray}.
Using the Numpy C-\acrshort{API}, I can effortlessly extract a C array pointer to the data buffer of any array and access it as usual from C code.
Therefore, I can maintain a valid NumPy \pythoninline{ndarray} object in Python while simultaneously providing the back end functions with a standard C array pointer to the exact same data.

Another aspect to discuss are the Python callback operations functions.
In principle, any custom Python function can be called using the Python/C \acrshort{API}.
However, the only way I can make use of \acrshort{API} calls when the back end processes an input task's steps is by adding a special tracker node to the network.
This node is defined by the C extension code and has two purposes.
On the one hand, the tasks' data pointers are wrapped by a new \pythoninline{ndarray} object so that they can be passed to the callback function.
On the other hand, the \cinline{PyObject} structure pointer to the callback function object is extracted from the \cinline{op_info} field of the processing tasks' steps and called with the newly created NumPy array as its only argument.
All of this is part of a single operation function that is registered with the special callback tracker node.
While this solution is easy to implement, it comes with a flaw that is acceptable because of the focus of \pythoninline{pyprocchain} to be a development tool.
The problem is that because all Python callbacks are processed through a single processing tracker node the sophisticated scheduling features of the back end are crippled.
For a processing network with tasks that only use Python callback operation functions, it is not possible to schedule the single tracker node responsible for the callbacks.
In fact, the first and only scheduling cycle triggers the evaluation of the callback node and completes when all tasks' steps are done.
Each time a task's step is successfully processed, the task is re-assigned to the current tracker node, at which point this cycle repeats until all steps are done.
However, the goal is not to optimize \pythoninline{pyprocchain} for these performance related aspects and, as previously discussed, I try to provide a simple interface instead.

Most of the methods of the \pythoninline{ProcNet} type are simple wrappers for the back end functions that access the required data of the Python objects and pass them on.
But, the Python interface for adding new input tasks is more complex.
By starting with the latter, I introduce all methods and discuss their implementations in the following.

\subsubsection*{\pythoninline{ProcNet.new_input_task}:}
This method combines a series of steps for setting up processing tasks.
The first parameter is the NumPy \pythoninline{ndarray} holding the data for the task.
The shape of these arrays is restricted to 1D so that any higher dimensional arrays can always be cast into this flattened shape.
This standardizes the interface to the data buffers.
Operation functions need to be aware of the layouts of the array pointers anyway and can temporally restore the original shape during processing.
Additionally, the current implementation only allows for two \pythoninline{numpy.dtypes} for the arrays: \pythoninline{numpy.uint32} which is equivalent to \cinline{unsigned int} in C and \pythoninline{numpy.float32} which is equivalent to \cinline{float} in C.
These two types are most commonly used for flight software applications and can easily be extended with new types from the C extension code.
Depending on the data type, it is necessary to choose different operation functions from the back end as they hardcode the casting of the array pointers.
The supplied NumPy \pythoninline{ndarray} is first tested for compliance with these restrictions and then the C array pointer to its data is saved to the C structure of the new input task.
Because of implementation details of the memory layout of NumPy arrays, it can happen that the supplied \pythoninline{ndarray} object is copied to ensure a C contiguous memory layout.
In this case, the supplied reference to the array object no longer points to the actual data that is modified during the processing steps.
Therefore, \pythoninline{ProcNet.new_input_task} returns a reference to the NumPy \pythoninline{ndarray} object that is guaranteed to hold the correct data at all times.
As a safety measure, a reference to this NumPy array object is always stored in the \pythoninline{ProcNet} instance's \pythoninline{output} attribute.
This prevents Python from garbage collecting the NumPy array and ensures that the previously extracted C array pointer is always valid.

The list of steps for the new tasks is provided via the \pythoninline{opcodes} parameter.
This list can contain the ID codes of operation functions available from the back end as well as the Python callback functions.
The callbacks must accept the NumPy \pythoninline{ndarray} object as their only argument and must return \pythoninline{None}.
Again, as a safety measure against garbage collection, a reference to all Python callback function objects is stored in a hidden instance attribute.
This attribute is a Python \pythoninline{set} so that there are no duplicate entries.

The rest of this method's parameters are optional and allow to configure the tasks' properties as discussed in \cref{sec:generic-proc-net}.
However, the \cinline{op_info} field is no longer available because it is already used to save a reference to the \cinline{PyObject} structure representing the Python callback function for the C interface.

\subsubsection*{\pythoninline{ProcNet.prepare_nodes}:}
This method populates a newly instantiated \pythoninline{ProcNet} processing network with additional tracker nodes.
After instantiation, the input and special output nodes are already added.
By calling this method on the instance, the special tracker node for processing the Python callback functions is added to the network.
For convenience, any operation function defined in the back end is also registerd with its tracker node so that managing nodes individually is no longer required.

\subsubsection*{\pythoninline{ProcNet.process_inputs, ProcNet.process_next, ProcNet.process_outputs}:}
These methods are just simple wrappers for their respective back end functions.
They also forward the return values of the wrapped functions back to Python.

\subsubsection*{\pythoninline{ProcNet} magic methods:}
Two magic methods of \pythoninline{ProcNet} are implemented explicitly in the C extension type.
The \pythoninline{__init__} method is used by Python to prepare new instances after their creation.
For \pythoninline{ProcNet}, the instantiation operator and thus \pythoninline{__init__} do not accept any arguments.
The fields of the \cinline{PyObject} structure that represents the instance are filled with the reference to the \cinline{proc_net} structure returned from the back when creating a new process network and an empty tuple and set for the \pythoninline{output} attribute and callback collection.
In this step, the input and special output node are also added to the process network.

The second method is the deallocation method that is used by Python when the instance object is garbage collected.
Here, the reference counts of any Python objects saved in the instance are decreased and the proper function for destroying the process network is called from the back end.
Note that if the only reference to a task's NumPy data array is saved in the \pythoninline{output} attribute, it will be deallocated together with the instance.

\subsubsection*{\vspace{-\baselineskip}}

\Cref{fig:pyproc-diags} represents a summary of the interface of \pythoninline{pyprocchain}.
It makes use of the Python type hint syntax to describe the objects' types.
Aside from \pythoninline{ProcNet}, the package also exposes a dictionary that contains the ID codes of all operation functions available from the back end so that they can be queried from Python.
There is also a \lstinline[style=myPython,basicstyle=\color{emph}\ttfamily\normalsize]!demo! module with Python code examples included.
These examples are discussed in \cref{app:python-ex}.

\pythoninline{pyprocchain} is packaged using \emph{setuptools} \parencite{setuptools}.
This automatically takes care of compiling the C extension code during installation and makes the resulting library accessible to the Python interpreter.
The setup requires to configure any external dependencies like the NumPy PIPY package and the library for the back end.
The source code is still to be added to the UVIE FlightOS repository at the time of writing.

% I want to point out that the result effectively reduced the number of functions for the interface compared to the back end while also providing developer-friendly simplifications.
