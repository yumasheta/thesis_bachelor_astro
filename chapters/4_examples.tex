% !TeX root = ../bac_thesis.tex

\chapter{Workflow Example}\label{ch:workflow}

In this section we go through a typical workflow for using \pythoninline{pyprocchain} step by step.
We start by setting up our Python environment from the terminal.
Our current working direcory has the following contents, which are all the required source code files from UVIE FlightOS for the back end as well as the Python C extension type \cinline{pyprocchainmodule.c} and package contents.
% \begin{noindent}
\vspace{-\baselineskip} % because last line of text block fill line completely
\medskip%
\begin{terminal}
	$\$$ tree .
	.
	|-- C_EXT_proc_chain_demo.c
	|-- include
	|   |-- data_proc_net.h
	|   |-- data_proc_op.h
	|   |-- data_proc_task.h
	|   |-- data_proc_tracker.h
	|   `-- list.h
	|-- lib
	|   |-- data_proc_net.c
	|   |-- data_proc_op.c
	|   |-- data_proc_task.c
	|   `-- data_proc_tracker.c
	|-- Makefile
	|-- pyprocchain_demo
	|   |-- fits_example.py
	|   |-- __init__.py
	|   `-- run_example.py
	|-- pyprocchainmodule.c
	|-- pyproject.toml
	|-- README.md
	|-- setup.cfg
	`-- setup.py
\end{terminal}
\medskip%
% \end{noindent}
The first step is to build the library required by \pythoninline{pyprocchain} from the back end source files.
The \cinline{make} target \cinline{deps} is pre-configured for this purpose.
% \begin{noindent}
\medskip%
\begin{terminal}
	$\$$ make deps
	...
\end{terminal}
% \medskip%
% \end{noindent}
% \begin{noindent}
% \medskip%
\begin{terminal}
	$\$$ ls lib/libdata_proc.so
	lib/libdata_proc.so
\end{terminal}
\medskip%
% \end{noindent}
The next step is to set up the Python environment.
We are using the built in \pythoninline{venv} module.
% \begin{noindent}
\medskip%
\begin{terminal}
	$\$$ python -m venv pyprocchain_venv
\end{terminal}
\medskip%
% \end{noindent}
After we activate the environment, we can use pip to install the \pythoninline{pyprocchain} package from the current working direcory.
% \begin{noindent}
\medskip%
\begin{terminal}
	$\$$ source pyprocchain_venv/bin/activate
\end{terminal}
% \medskip%
% \end{noindent}
% \begin{noindent}
% \medskip%
\begin{terminal}
	(pyprocchain_venv) $\$$ pip install .
	...
\end{terminal}
% \medskip%
% \end{noindent}
% \begin{noindent}
% \medskip%
\begin{terminal}
	(pyprocchain_venv) $\$$ pip list
	Package     Version
	----------- -------
	numpy       1.21.0
	pip         21.1.1
	pyprocchain 1.0
	setuptools  56.0.0
\end{terminal}
\medskip%
% \end{noindent}
At this point \pythoninline{pyprocchain} is ready to use.
We continue with Jupyter-notebook like cells.
It is important to evoke the interpreter from the current working directory because the back end library is linked as a path relative to this directory.
First we import the NumPy module and the Python bindings.
Then we instantiate a new process network and set up all tracker nodes.
% \begin{noindent}
\vspace{-\baselineskip} % because last line of text block fill line completely
\medskip%
\begin{ipython}
!*\ipythonprompt\vspace{-\baselineskip}*!
	import numpy as np
	import pyprocchain
\end{ipython}
% \medskip%
% \end{noindent}
% \begin{noindent}
% \medskip%
\begin{ipython}
!*\ipythonprompt\vspace{-\baselineskip}*!
	pn = pyprocchain.ProcNet()
	pn.prepare_nodes()
\end{ipython}
\medskip%
% \end{noindent}
We want to develop a new operation function and start with an empty template.
% \begin{noindent}
\medskip%
\begin{ipython}
!*\label{ipy:opfunc}\ipythonprompt\vspace{-\baselineskip}*!
	def op_func(data: np.ndarray):
		pass
\end{ipython}
\medskip%
% \end{noindent}
Before we fill out the template, we prepare the rest of the code.
One cell contains the setup of the new input task.
We make sure to save a reference to the returned NumPy array for later.
Another cell combines all scheduling cycles in a loop and prints the output.
% \begin{noindent}
\medskip%
\begin{ipython}
!*\label{ipy:task}\ipythonprompt\vspace{-\baselineskip}*!
	op_list = [
		op_func,
	]

	output = pn.new_input_task(
		np.arange(10, dtype=np.uint32),
		op_list,
	)

	pn.process_inputs()
\end{ipython}
\medskip%
% \end{noindent}
% \begin{noindent}
\medskip%
\begin{ipython}
!*\label{ipy:out}\ipythonprompt\vspace{-\baselineskip}*!
	for cnt in iter(pn.process_next, 0):
		print(f"==>{cnt} tasks executed for current node!\n")
	else:
		pn.process_outputs()

	print(f"{output=}")
\end{ipython}
\medskip%
% \end{noindent}
Executing the above just prints out the input array.
We now change the code in \cref{ipy:opfunc} to the one below and add some functionality.
Then we execute \cref{ipy:opfunc} and all subsequent \cref{ipy:task,ipy:out} again and inspect the output.
We can repeat this cycle until our new operation functions is completed.
% \begin{noindent}
\medskip%
\begin{ipython}
!*\ipythonprompt\vspace{-\baselineskip}*!
	def op_func(data: np.ndarray):
		data[:-1] += data[1:]
		data[-1] += len(data)
\end{ipython}
\medskip%
% \end{noindent}
We can query the ID codes of operation functions that are implemented in the back end by accessing the \pythoninline{pyprocchain.OPCODES} attribute.
The names of the dictionary keys are set from the back end code.
% \begin{noindent}
\medskip%
\begin{ipython}
!*\ipythonprompt\vspace{-\baselineskip}*!
	op_list = [
		op_func,
		pyprocchain.OPCODES["OP_ADD_INT"],
	]
\end{ipython}
\medskip%
% \end{noindent}
If we want to add our new operation function to the back end code, we have to modify the \mbox{\cinline{lib/data_proc_op.c}} file and its header file in \cinline{include/}.
We need to add the C equivalent code as a new function and update the \cinline{pn_get_op_codes_dict} and \cinline{pn_prepare_nodes} functions in the same file.
These functions provide the items of the \pythoninline{pyprocchain.OPCODES} dictionary and register all operation functions from the back end with a new processing network when calling \pythoninline{ProcNet.prepare_nodes} on an instance.
Because we have changed the back end source code, we have to re-compile the library, which \pythoninline{pyprocchain} depends on, using the same \cinline{make deps} command as before.
We do not need to reinstall the \pythoninline{pyprocchain} package because we only link to the back end library.
We should, however, restart any running IPython kernels to make sure the new library is loaded.

% terminal cell template
% \begin{noindent}
% \medskip%
% \begin{terminal}
% 	$\$$ 
% \end{terminal}
% \medskip%
% \end{noindent}

% ipython cell template
% \begin{noindent}
% \medskip%
% \begin{ipython}
% !*\label{ipy:}\ipythonprompt\vspace{-\baselineskip}*!
% 	some python code
% \end{ipython}
% \medskip%
% \end{noindent}
