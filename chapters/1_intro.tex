% !TeX root = ../bac_thesis.tex

\chapter{Introduction}\label{ch:intro}

Space observatories play an important role in observational astronomy.
Spacecraft loaded with scientific instruments are sent to a specific destination in our solar system or placed in various orbits around the Earth itself.
These remote locations offer the possibility to escape the disturbing influences of the Earth's atmosphere, among many other advantages.
An elaborate discussion about the benefits and drawbacks of space observatories is provided by \textcite{space-observatories,space-obs-optical}.

Aside from the astronomical instruments themselves, the spacecraft contain electronics systems and innovative technologies that make the endeavor feasible and safe.
As part of those, the flight software is responsible for instrument control and data handling.
At the core of the work at hand lies the UVIE FlightOS%
\footnote{
	\raggedright%
	UVIE FlightOS was formerly known as LeanOS.\@
	A development snapshot is hosted at: \url{https://gitlab.phaidra.org/luntzea4/flightos} (visited on 05/2021).
},
a flight software developed by the space instrumentation working group at the Department of Astrophysics of the \acrfull{UVIE}.
In the subsequent chapters, I explore the capabilities and implementation details of the UVIE FlightOS with the example of two selected payload contributions.
I devote special attention to the process chain of the flight software and its use for on-board science data processing.
In the main \cref{ch:py-bindings}, I analyze the C source code of the process chain and discuss how Python bindings can improve the development workflow.
I then present \pythoninline{pyprocchain} which implements these bindings using a Python C extension type.
Finally, I provide detailed steps with examples that build up the typical workflow when using \pythoninline{pyprocchain} to develop new process chains for UVIE FlightOS.\@


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	section:	The CHEOPS and SMILE Missions
%	label:		sec:missions-intro
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[The CHEOPS and SMILE Missions]{The \acrshort{CHEOPS} and \acrshort{SMILE} Missions}\label{sec:missions-intro}

In the following, I introduce two selected missions that are tightly coupled to the UVIE FlightOS and lend themselves as illustrative examples in later chapters.
In order to understand how the software works, it is important to get familiar with the special hardware hosting it.
The first example is the \acrfull{CHEOPS} that is under the responsibility of the Swiss-led \acrshort{CHEOPS} Mission Consortium as part of the \acrfull{ESA}.
The start of the project dates back to 2012 and the satellite launched on December 18th, 2019.
It is currently operating in its science phase after the completion of commissioning in March of 2020.
The objective of \acrshort{CHEOPS} is to perform ultra-high precision photometry measurements of transit events for known exoplanets.
For this purpose, the spacecraft is fitted with a single instrument.
The \acrfull{CIS} houses an on-axis Ritchey-Chrétien telescope with an aperture of 33 cm.
A visualization of the \acrshort{CIS} is included in \cref{fig:cheops-cis}.
All details about \acrshort{CHEOPS} are compiled in the "Definition Study Report" \parencite{cheops-red_book} and the recent "CHEOPS mission" report by \textcite{cheops-mission}.

Two main electronics systems are part of the \acrshort{CIS}.
Directly attached to the telescope's \acrshort{CCD} is the \acrfull{SEM}.
Its tasks are to read out the \acrshort{CCD} and the housekeeping for the telescope.
In general, it represents the camera of \acrshort{CHEOPS} and is controlled by the \acrfull{BEE}.
As explained by \textcite{cordet-framework} the \acrshort{BEE} consist of a redundant set of \acrfullpl{DPU}.
Developed by the Institute for Space Research Graz, the \acrshort{DPU} is a Gaisler GR712RC system on chip with two Leon3 cores and SpaceWire cores.
The SpaceWire link is used to control the \acrshort{SEM} and communication with the \acrfull{OBC} is based on a MIL-1553 link.
The UVIE FlightOS takes the role of the \acrfull{IFSW} that is running on the DPU and implements the centroid measurements for the fine guiding of the spacecraft as well as the science data compression for the downlink to Ground as part of its process chain.

Another mission that will make use of the UVIE FlightOS is the \acrfull{SMILE}.
It was kicked off in 2015 as a collaboration between \acrshort{ESA} and the Chinese Academy of Science where both parties share the mission and science operation.
Because the launch is scheduled for 2023 its development is still ongoing.
This allows to implement and use the latest features of UVIE FlightOS.\@
As its name suggests the science goals of \acrshort{SMILE} are to observe the interaction between the solar wind and the Earth's magnetosphere.
For this purpose, the satellite (\cref{subfig:smile-spacecraft}) will house many different instruments of which only the \acrfull{SXI}, which is depicted in \cref{subfig:smile-sxi}, will make use of the UVIE FlightOS.\@
The detectors are \acrshortpl{CCD} sensitive to X-rays in the energy range of ca.\ 0.15 to 2.5 keV.
The \acrshort{SXI} has its own, dedicated electronics systems.
Compared to \acrshort{CHEOPS}, the \acrfull{FEE} take the place of the \acrshort{SEM}.\@
The \acrshort{DPU} will be the same GR712RC that was already used for \acrshort{CHEOPS} and will host the \acrshort{IFSW} that will be responsible for commanding the \acrshort{FEE}, support diagnostics and data compression for the downlink to Ground \parencite{smile-iasw}.
All details about \acrshort{SMILE} are compiled in the "Definition Study Report" \parencite{smile-red_book}.


\begin{figure}[p]
	\centering
	\includegraphics[width=0.4\textwidth]{img_src/cheops_CIS.png}
	\caption[The \acrfull{CIS} with its \acrfull{SEM} and \acrfull{BEE}.]{
		The \acrfull{CIS} that contains the telescope as well as the \acrfull{SEM} and \acrfull{BEE}.
		The \acrfull{DPU} is part of the BEE and is running FlightOS.\\
		\url{https://sci.esa.int/s/AGq5BrW} (visited on 05/2021)}%
	\label{fig:cheops-cis}
\end{figure}
\begin{figure}[p]
	\centering
	\begin{subfigure}[b]{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{img_src/smile_spacecraft.png}
		\caption{Artist's impression of the SMILE spacecraft.\\
			\url{https://sci.esa.int/s/WvPEQgA}\\
			(visited on 05/2021)}%
		\label{subfig:smile-spacecraft}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{img_src/smile_SXI.png}
		\caption{The \acrfull{SXI} of SMILE.\\
			\url{https://sci.esa.int/s/ApP0K4A}\\
			(visited on 05/2021)}%
		\label{subfig:smile-sxi}
	\end{subfigure}
	\caption[The SMILE spacecraft and \acrfull{SXI}.]{
		The SXI (\subref{subfig:smile-sxi}) is one of the instruments on board of the SMILE spacecraft (\subref{subfig:smile-spacecraft})
		and will be used to measure the X-rays that are emitted from accelerated, charged particles in the Earth's magnetosphere.
		Figure (\subref{subfig:smile-sxi}) highlights the \acrfull{FEE} and \acrfull{BEE}, the latter of which contains the \acrfull{DPU} that will be running FlightOS.%
	}\label{fig:smile-sxi-viz}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	section:	Components of IFSW
%	label:		sec:components-ifsw
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Components of IFSW]{Components of \acrfull{IFSW}}\label{sec:components-ifsw}

Before diving into the details of UVIE FlightOS, I come forward with a summary of the different components that are common for \acrshort{IFSW}.
The primary task is to safely control the instrument together with supplementary hardware such as shutters, power supplies and the \acrshort{SEM} or \acrshort{FEE}.
For this, communicating with the electronics modules as well as the \acrshort{OBC} is necessary and the data acquired from the sensors need to be handled accordingly.
Maintaining the health of the instrument requires \acrfull{FDIR} together with logging diagnostics and gathering housekeeping data.
A similar list with more tasks specific to the needs of the \acrshort{SXI} is discussed by \textcite{smile-sxi-onboard}.

How these components are implemented as part of the flight software will vary based on the target \acrshortpl{DPU} architecture.
A comprehensive discussion for the UVIE FlightOS and the \acrfull{MPPB} is carried out by \textcite{amicsa-dsp-day_2016_ssdp}.
That special hardware is developed by Recore Systems for \acrshort{ESA} \parencite{dsp_day_2012_MPPB}.
Regardless, many aspects still apply for the Leon3 based \acrshortpl{DPU} mentioned earlier.
They boil down to the following for the core \acrfull{OS}.
An efficient scheduler for threading is required to eliminate stalling or bottlenecks.
A part of the scheduler is multi-core support with synchronization and locking.
Proper event handling is achieved with hardware interrupts and \acrshort{CPU} traps.
The generation of logs and housekeeping data relies on access to internal attributes via an interface that is provided by the OS.\@
On top of the \acrshort{OS}, drivers are needed to operate the SpaceWire and MIL-1553 link devices and offer \acrshort{FDIR}.
Communication to and control from Ground is handled by \acrfull{PUS} \parencite{pus-standard} services in compliance with the \acrfull{ECSS}.
At the highest level, the flight software will provide limited, on-board science data processing.
Commonly, the requirement for these high-level tasks is the compression of the collected observation data.
More advanced tasks, like the fine guiding of \acrshort{CHEOPS}, are also possible but highly tailored towards one specific mission \parencite{cheops-fine-guiding}.
