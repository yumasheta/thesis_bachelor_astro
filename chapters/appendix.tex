% !TeX root = ../bac_thesis.tex


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	chapter:	Generic Processing Network Example
%	label:		app:generic-proc-net
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Generic Processing Network Example}\label{app:generic-proc-net}
\vspace{-\baselineskip}
% \begin{noindent}
	\begin{C}[caption={[Example C code for building a generic processing network that is
		targeted for the PC platform.]%
		Example C code for building a generic processing network that is
		targeted for the PC platform.
		First, the special operation function \pythoninline{op_output} is defined.
		It is registered with the output node of the processing network on line 92 and prints the results to the console and cleans up the processing task.
		The second operation function \pythoninline{op_add} represents a simple processing step.
		The \pythoninline{main} function of the program demonstrates the manual administration of the processing network.
		The network has to be created and populated with tracker nodes for each operation function.
		Then a simple input task is prepared and fed into the network.
		Scheduling is taken care of manually by explicitly processing the next tracker node in a loop.
		Lastly, the output node is processed and the network is cleaned up.
		The \pythoninline{BUG_ON} macro triggers configurable error handling in the case that the enclosed functions return error codes.}]
	/**
	* First compile the dependencies:
	* $ gcc -Iinclude -O2 -fPIC -shared -Wl,-soname,libdata_proc.so lib/data_proc_net.c lib/data_proc_op.c lib/data_proc_task.c lib/data_proc_tracker.c -o lib/libdata_proc.so -lc
	*
	* Then this example file:
	* $ gcc -Iinclude -O2 -L lib -ldata_proc -Wl,-rpath,./lib generic_proc_chain_demo.c lib/libdata_proc.so -o generic_proc_chain_demo
	* 
	* The `include` and `lib` folders contain header and library files found at https://github.com/aluntzer/flightos.
	*/
	
	#include <stdlib.h>
	#include <stdarg.h>
	#include <sys/types.h>
	/* dependencies from UVIE FlightOS */
	#include <data_proc_task.h>
	#include <data_proc_tracker.h>
	#include <data_proc_net.h>

	/* constants for tracker nodes */
	#define CRIT_LEVEL 10
	#define OP_ADD 0x1234
	!*\newpage*!
	/**
	* special output node. prints the data to the console.
	*/
	int op_output(unsigned long op_code, struct proc_task *t)
	{
		ssize_t i;
		ssize_t n;
		unsigned int *p = NULL;

		n = pt_get_nmemb(t);
		printf("OUT: op code %x, %d items\n", op_code, n);

		if (!n)
			goto exit;

		p = (unsigned int *)pt_get_data(t);
		if (!p)
			goto exit;

		for (i = 0; i < n; i++) {
			printf("\t%d\n", p[i]);
		}

	exit:
		free(p); /* clean up our data buffer */
		pt_destroy(t);

		return PN_TASK_SUCCESS;
	}
	
	/**
	* example processing node. adds 10 to all data elements.
	*/
	int op_add(unsigned long op_code, struct proc_task *t)
	{
		ssize_t i;
		ssize_t n;
		unsigned int *p;

		n = pt_get_nmemb(t);
		if (!n)
			return PN_TASK_SUCCESS;

		p = (unsigned int *)pt_get_data(t);

		if (!p) /* we have elements but data is NULL, error*/
			return PN_TASK_DESTROY;

		printf("ADD: op code %x, %d items\n", op_code, n);

		for (i = 0; i < n; i++) {
			p[i] += 10;
		}

		return PN_TASK_SUCCESS;
	}
	!*\newpage*!

	int main(int argc, char **argv)
	{
		/* create a new process network */
		struct proc_net *pn = pn_create();
		BUG_ON(!pn);

		/* create the tracker node for the processing op "add" */
		struct proc_tracker *pt = pt_track_create(op_add, OP_ADD, CRIT_LEVEL);
		BUG_ON(!pt);
		BUG_ON(pn_add_node(pn, pt));
		/* and the special output node */
		BUG_ON(pn_create_output_node(pn, op_output));

		/* create a new input task */
		int steps = 3;
		int type = 0;
		int seq = 42;
		struct proc_task *t = pt_create(NULL, 0, steps, type, seq);
		BUG_ON(!t);
		/* add exactly `steps` number of operation steps */
		BUG_ON(pt_add_step(t, OP_ADD, NULL));
		BUG_ON(pt_add_step(t, OP_ADD, NULL));
		BUG_ON(pt_add_step(t, OP_ADD, NULL));

		/* prepare data */
		int n = 32;
		unsigned int *data = calloc(n, sizeof(unsigned int));
		for (int i = 0; i < n; i++)
			data[i] = i;
		/* register data with input task */
		pt_set_data(t, data, n * sizeof(unsigned int));
		pt_set_nmemb(t, n);

		/* add the new input task to the process network */
		pn_input_task(pn, t);
		/* process the input task, assign it to its first node */
		pn_process_inputs(pn);
	
		/* dynamically schedule and process the nodes of the process network */
		do {
			printf("==> processing next proc_tracker\n");
		} while (pn_process_next(pn));
		printf("==> all processing trackers are empty\n");
		/* process all input tasks that reached the output node */
		pn_process_outputs(pn);
		/* clean up */
		pn_destroy(pn);

		return 0;
	}
	\end{C}
% \end{noindent}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	chapter:	Python Usage Example
%	label:		app:python-ex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Python Usage Examples}\label{app:python-ex}

The example Python code in this section demonstrates the use of all the features of \pythoninline{pyprocchain}.
In doing so, the specific configuration of these processing networks and the operation functions they contain are not representative of actual data processing pipelines used for flight software.
Still, they contain all parts in a simplified form.

The first \cref{lst:pyex1} configures two tasks with \pythoninline{numpy.uint32} and \pythoninline{numpy.float32} data types.
Their lists of steps uses the same Python callback operation function that is also defined in the listing.
Here, it is not necessary to distinguish between the data types.
However, for the selected back end operation functions it is necessary to choose the ID code for the function that expects the matching data type.
The output for the two NumPy arrays will be identical.
This demonstrates the seamless combination of back end and Python callback operation functions.

The second \cref{lst:pyex2} makes use of the \pythoninline{astropy} package to read the data for the input task from a FITS file.
It is important to properly extract the NumPy array with the correct \pythoninline{numpy.dtype} and then to flatten the array to the 1D shape.
Similarly, inside the Python callback operation function one needs to make sure that the data array is modified in place.
In the case of the \pythoninline{astropy.convolution.convolve} function, a new NumPy array is returned and needs to be copied to the memory location of the input array.
The same is true when modifying the shape of the \pythoninline{ndarray}.
In the end, the output is written back to a FITS file that will be the blurred version of the original image.

% \begin{noindent}
	\begin{python}[caption={%
		Python usage example with different \pythoninline{dtypes} and
		mixing Python callbacks with OPs implemented in the C back end.%
		},label={lst:pyex1}]
	"""Using OPs from the C back end and Python callbacks."""
	
	import numpy as np
	import pyprocchain
	
	
	def op_add(arr):
		np.add(arr, 10, arr)  # this works with any dtype
	
	
	pn = pyprocchain.ProcNet()
	pn.prepare_nodes()

	op_list_int = [
		op_add,
		pyprocchain.OPCODES["OP_SUB_INT"],
		pyprocchain.OPCODES["OP_MUL_INT"],
	]
	op_list_float = [
		op_add,
		pyprocchain.OPCODES["OP_SUB_FLOAT"],
		pyprocchain.OPCODES["OP_MUL_FLOAT"],
	]

	# np dtype `unint32`
	out1 = pn.new_input_task(np.arange(5, dtype=np.uint32), op_list_int)

	# np dtype `float32'
	out2 = pn.new_input_task(np.arange(5, dtype=np.float32), op_list_float)

	pn.process_inputs()

	for cnt in iter(pn.process_next, 0):
		print(f"==> {cnt} tasks executed for current node!\n")
	else:
		pn.process_outputs()

	print(f"outputs:\n{out1=}\n{out2=}\n")
\end{python}
% \end{noindent}

% \begin{noindent}
	\begin{python}[caption={%
		Python usage example accessing fits files via \pythoninline{astropy}.%
		},label={lst:pyex2}]
	"""Example using FITS input.

	This script requires the astropy pypi package.
	"""
	
	import numpy as np
	import pyprocchain
	
	from astropy.io import fits
	from astropy.convolution import Gaussian2DKernel, convolve
	from astropy.utils.data import get_pkg_data_filename
	
	
	def gauss_blur(data):
		"""Convolution with gaussion kernel, aka. gaussian blur."""
		# don't use reshape, as it could create a copy!
		data.shape = (893, 891)
		kernel = Gaussian2DKernel(x_stddev=20, y_stddev=20)
		# explicitly copy result to input data array, convolve returns new
		np.copyto(data, convolve(data, kernel).astype(np.uint32))
		# restore 1D shape
		data.shape = (np.prod(data.shape),)


	# load sample image from astropy
	image_file = get_pkg_data_filename('tutorials/FITS-images/HorseHead.fits')
	# type conversion is important!
	data = fits.getdata(image_file, ext=0).astype(np.uint32)
	# reshape to 1D!
	orig_shape = data.shape
	data = np.reshape(data, np.prod(data.shape))
	
	pn = pyprocchain.ProcNet()
	pn.prepare_nodes()
	output = pn.new_input_task(data, [gauss_blur])
	pn.process_inputs()
	pn.process_next()
	pn.process_outputs()

	hdu = fits.PrimaryHDU(np.reshape(output, orig_shape))
	hdu.writeto('blurred_image.fits')

\end{python}
% \end{noindent}